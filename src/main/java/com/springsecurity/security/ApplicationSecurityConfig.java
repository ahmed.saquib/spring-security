package com.springsecurity.security;

import com.springsecurity.auth.ApplicationUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
class ApplicationSecurityConfig extends WebSecurityConfigurerAdapter {

    private final PasswordEncoder passwordEncoder;
    private final ApplicationUserService applicationUserService;

    @Autowired
    public ApplicationSecurityConfig(PasswordEncoder passwordEncoder, ApplicationUserService applicationUserService) {
        this.passwordEncoder = passwordEncoder;
        this.applicationUserService = applicationUserService;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // Authorize request for any request must be authenticated, and we want to use basic
        // authentication
        http.csrf()
            .disable()
            .authorizeRequests()
            .antMatchers("/", "index", "/css/*, /js/*")
            .permitAll()
            .antMatchers("/login*")
            .permitAll()
            .anyRequest()
            .authenticated()
            .and()
            .formLogin()
            .loginPage("/login")
            .defaultSuccessUrl("/home", true)
            // .passwordParameter("password")
            // .usernameParameter("username")
            .and()
            .rememberMe() // default 2 weeks
            // .rememberMeParameter("remember-me")
            .and()
            .logout()
            .logoutUrl("/logout")
            .clearAuthentication(true)
            .deleteCookies("JSESSIONID", "remember-me")
            .invalidateHttpSession(true)
            .logoutSuccessUrl("/login");
    }

    // UserDetailsService is how you get user from Database.
    // for now, we will just simply define a user, later we will collect user data from the database.
//    @Override
//    @Bean
//    protected UserDetailsService userDetailsService() {
//        UserDetails userSaquib =
//            User.builder()
//                .username("Saquib")
//                .password(passwordEncoder.encode("password"))
//                .authorities("GET")
//                .build();
//        UserDetails userAdmin =
//            User.builder()
//                .username("Admin")
//                .password(passwordEncoder.encode("password"))
//                .authorities("GET", "POST", "DELETE", "PUT")
//                .build();
//        return new InMemoryUserDetailsManager(userSaquib, userAdmin);
//    }


    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(daoAuthenticationProvider());
    }

    @Bean
    public DaoAuthenticationProvider daoAuthenticationProvider(){
        DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
        provider.setPasswordEncoder(passwordEncoder);
        provider.setUserDetailsService(applicationUserService);
        return provider;
    }
}
