package com.springsecurity.auth;

import java.util.Optional;

public interface ApplicationUserDao {
    public Optional<ApplicationUsers> selectApplicationUserByUsername(String username);
}
