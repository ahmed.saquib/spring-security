package com.springsecurity.auth;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class fakeApplicationDaoService implements ApplicationUserDao{

    private final PasswordEncoder passwordEncoder;

    public fakeApplicationDaoService(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public Optional<ApplicationUsers> selectApplicationUserByUsername(String username) {
        return getApplicationUsers().stream().filter(applicationUsers -> username.equals(applicationUsers.getUsername())).findFirst();
    }

        List<GrantedAuthority> admin_authorities = new ArrayList<>();
    private List<GrantedAuthority> getAdmin_authorities(){
        admin_authorities.add(new SimpleGrantedAuthority("POST"));
        return admin_authorities;
    }

    private List<ApplicationUsers> getApplicationUsers(){
        List<ApplicationUsers> applicationUsers = List.of(
                new ApplicationUsers(
                        getAdmin_authorities(),
                        passwordEncoder.encode("password"),
                        "Admin",
                        true,
                        true,
                        true,
                        true
                ));
          return applicationUsers;
      }
}
