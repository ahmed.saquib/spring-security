package com.springsecurity.entity;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Student {

    private Integer studentId;
    private String studentName;
}
