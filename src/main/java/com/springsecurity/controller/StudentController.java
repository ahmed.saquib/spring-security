package com.springsecurity.controller;

import com.springsecurity.entity.Student;
import org.springframework.security.access.prepost.PreAuthorize;
import java.util.List;
import java.util.Arrays;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("v1")
class StudentController {

    private static final List<Student> students = Arrays.asList(new Student(1, "Saquib"), new Student(2, "Emon"),
        new Student(3, "Tanisha"));

    @GetMapping(path = "/get_student/{studentId}")
    @PreAuthorize("hasAnyAuthority('GET')")
    public Student getStudent(final @PathVariable("studentId") Integer studentId) {
        return students.stream().filter(student -> studentId.equals(student.getStudentId())).findFirst()
            .orElseThrow(() -> new IllegalStateException("Student id " + studentId + " does not exist!"));
    }

    @GetMapping("/get_students")
    @PreAuthorize("hasAnyAuthority('GET')")
    public List<Student> getStudents() {
        return students;
    }

    @PostMapping("/add_student")
    @PreAuthorize("hasAnyAuthority('POST')")
    public void registerNewStudent(@RequestBody Student student) {
        System.out.println(student);
    }

    @DeleteMapping(path = "/delete_student/{studentId}")
    @PreAuthorize("hasAnyAuthority('DELETE')")
    public void deleteStudent(@PathVariable Integer studentId) {
        System.out.println(studentId);
    }

    @PutMapping(path = "/update_student/{studentId}")
    @PreAuthorize("hasAnyAuthority('PUT')")
    public void updateStudent(@PathVariable Integer studentId, @RequestBody Student student) {
        System.out.printf("%s, %s", studentId, student);
    }
}